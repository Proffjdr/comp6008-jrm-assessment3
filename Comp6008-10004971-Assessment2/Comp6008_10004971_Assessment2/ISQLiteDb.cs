﻿using SQLite;

namespace Comp6008_10004971_Assessment2
{
    public interface ISQLiteDb
    {
        SQLiteAsyncConnection GetConnection();
    }
}

