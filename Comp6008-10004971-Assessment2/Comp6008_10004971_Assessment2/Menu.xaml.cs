﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Comp6008_10004971_Assessment2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : ContentPage
    {
        public Menu()
        {
            InitializeComponent();
        }

        //Tests input for number of images
        private int TestInput()
        {
            int i;
            if (!int.TryParse(NumImg.Text, out i) || i < 1)
            {
                i = 20;
            }
            return i;
        }

        //Launches main page to view images
        async void btnView_Clicked(object sender, EventArgs e)
        {
            int i = TestInput();
            await Navigation.PushAsync(new MainPage("Display", i));
        }

        //Launches main page to select image to edit
        async void btnEdit_Clicked(object sender, EventArgs e)
        {
            int i = TestInput();
            await Navigation.PushAsync(new MainPage("Edit", i));
        }

        //Launches main page to select image to delete
        async void btnDelete_Clicked(object sender, EventArgs e)
        {
            int i = TestInput();
            await Navigation.PushAsync(new MainPage("Delete", i));
        }

        //Launches add page
        async void btnAdd_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Add(-1));
        }
    }
}
