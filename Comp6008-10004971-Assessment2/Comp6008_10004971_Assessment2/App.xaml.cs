﻿using Xamarin.Forms;

namespace Comp6008_10004971_Assessment2

{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Welcome())
            {
                BarBackgroundColor = Color.FromHex("#06c"),
                BarTextColor = Color.FromHex("#fff")
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
