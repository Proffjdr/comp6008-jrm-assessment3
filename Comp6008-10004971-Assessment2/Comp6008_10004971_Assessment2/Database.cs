﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;

namespace Comp6008_10004971_Assessment2
{
    class Database
    {
        //Connecting to database
        private static SQLiteAsyncConnection _connection;

        //list of the movie details
        private static List<MovieDetails> Movies;

        //Setup
        static Database()
        {
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        //This method makes the database table
        public static async Task MakeTable()
        {
            //Create the table
            await _connection.CreateTableAsync<MovieDetails>();
            Movies = await _connection.Table<MovieDetails>().ToListAsync();
        }

        //This method deletes all movies
        public static async Task DeleteMovies()
        {
            //delete existing movies
            Movies = await _connection.Table<MovieDetails>().ToListAsync();

            for (int i = 0; i < Movies.Count; i++)
            {
                var Item = Movies[i];
                await _connection.DeleteAsync(Item);
            }
        }

        //This method adds all of the premade movies to the database
        public static async Task CreateMovies()
        {
            //declaring detail list
            List<Tuple<string, string, string, string, string>> Details = new List<Tuple<string, string, string, string, string>>();

            //adding to the list
            Details.Add(Tuple.Create("Summer Wars", "2009", "A student tries to fix a problem he accidentally caused in OZ, a digital world, while pretending to be the fiancé of his friend at her grandmother's 90th birthday.", "7.6/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTYyOTk3OTQzM15BMl5BanBnXkFtZTcwMjU4NDYyNA@@._V1_SY1000_CR0,0,681,1000_AL_.jpg"));
            Details.Add(Tuple.Create("Your Name", "2016", "Two strangers find themselves linked in a bizarre way. When a connection forms, will distance be the only thing to keep them apart?", "8.6/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BODRmZDVmNzUtZDA4ZC00NjhkLWI2M2UtN2M0ZDIzNDcxYThjL2ltYWdlXkEyXkFqcGdeQXVyNTk0MzMzODA@._V1_SY1000_SX675_AL_.jpg"));
            Details.Add(Tuple.Create("A Silent Voice", "2016", "The story revolves around Nishimiya Shoko, a grade school student who has impaired hearing.", "8.3/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BM2Y1YmQ5YzItNDIxMi00YTAxLThkMTctNTg3M2EwOTg0NWQwXkEyXkFqcGdeQXVyMjI5MjU5OTI@._V1_.jpg"));
            Details.Add(Tuple.Create("The Garden of Words", "2013", "A 15-year-old boy and 27-year-old woman find an unlikely friendship one rainy day in the Shinjuku Gyoen National Garden.", "7.6/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BNGYwMzQ5MjAtYWFjMy00ZTc1LTlkODQtM2Q5YzYzOWVkYTdhXkEyXkFqcGdeQXVyNjY1OTY4MTk@._V1_SY1000_CR0,0,715,1000_AL_.jpg"));
            Details.Add(Tuple.Create("When Marnie Was There", "2014", "Upon being sent to live with relatives in the countryside, an emotionally distant adolescent girl becomes obsessed with an abandoned mansion and infatuated with a girl who lives there - a girl who may or may not be real.", "7.8/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ1ODY3MTc5MF5BMl5BanBnXkFtZTgwOTUyNDI2NTE@._V1_.jpg"));
            Details.Add(Tuple.Create("Wolf Children", "2012", "College student Hana falls in love with another student who turns out to be a werewolf, who dies in an accident after their second child. Hana moves to the rural countryside where her husband grew up to raise her two werewolf children.", "8.2/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTUzNTUzMTA5OF5BMl5BanBnXkFtZTgwOTg0ODc1MTE@._V1_SY1000_CR0,0,675,1000_AL_.jpg"));
            Details.Add(Tuple.Create("Kiki's Delivery Service", "1989", "A young witch, on her mandatory year of independent life, finds fitting into a new community difficult while she supports herself by running an air courier service.", "7.9/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BOTc0ODM1Njk1NF5BMl5BanBnXkFtZTcwMDI5OTEyNw@@._V1_.jpg"));
            Details.Add(Tuple.Create("Ponyo", "2008", "A five year-old boy develops a relationship with Ponyo, a goldfish princess who longs to become a human after falling in love with him.", "7.7/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMjA5NzkxNTg2MF5BMl5BanBnXkFtZTcwMTA3MjU1Mg@@._V1_.jpg"));
            Details.Add(Tuple.Create("The Secret World of Arrietty", "2010", "The Clock family are four-inch-tall people who live anonymously in another family's residence, borrowing simple items to make their home. Life changes for the Clocks when their daughter, Arrietty, is discovered.", "7.6/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTAxNjk3OTYyODReQTJeQWpwZ15BbWU3MDgyODY2OTY@._V1_SY1000_CR0,0,674,1000_AL_.jpg"));
            Details.Add(Tuple.Create("Castle in the Sky", "1986", "A young boy and a girl with a magic crystal must race against pirates and foreign agents in a search for a legendary floating castle.", "8.1/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BNTg0NmI1ZGQtZTUxNC00NTgxLThjMDUtZmRlYmEzM2MwOWYwXkEyXkFqcGdeQXVyMzM4MjM0Nzg@._V1_.jpg"));
            Details.Add(Tuple.Create("My Neighbor Totoro", "1988", "When two girls move to the country to be near their ailing mother, they have adventures with the wondrous forest spirits who live nearby.", "8.2/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMjE3NzY5ODQwMV5BMl5BanBnXkFtZTcwNzY1NzcxNw@@._V1_.jpg"));
            Details.Add(Tuple.Create("Howl's Moving Castle", "2004", "When an unconfident young woman is cursed with an old body by a spiteful witch, her only chance of breaking the spell lies with a self-indulgent yet insecure young wizard and his companions in his legged, walking castle.", "8.2/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTY1OTg0MjE3MV5BMl5BanBnXkFtZTcwNTUxMTkyMQ@@._V1_SY1000_CR0,0,674,1000_AL_.jpg"));
            Details.Add(Tuple.Create("Spirited Away", "2001", "During her family's move to the suburbs, a sullen 10-year-old girl wanders into a world ruled by gods, witches, and spirits, and where humans are changed into beasts.", "8.6/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTk3NTM1NTg1Ml5BMl5BanBnXkFtZTgwOTgzMTMyMDE@._V1_SY1000_CR0,0,722,1000_AL_.jpg"));
            Details.Add(Tuple.Create("Princess Mononoke", "1997", "On a journey to find the cure for a Tatarigami's curse, Ashitaka finds himself in the middle of a war between the forest gods and Tatara, a mining colony. In this quest he also meets San, the Mononoke Hime.", "8.4/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTVlNWM4NTAtNDQxYi00YWU5LWIwM2MtZmVjYWFmODZiODE5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,707,1000_AL_.jpg"));
            Details.Add(Tuple.Create("The Wind Rises", "2013", "A look at the life of Jiro Horikoshi, the man who designed Japanese fighter planes during World War II.", "7.8/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTU4NDg0MzkzNV5BMl5BanBnXkFtZTgwODA3Mzc1MDE@._V1_SY1000_SX675_AL_.jpg"));
            Details.Add(Tuple.Create("Tales From Earthsea", "2006", "In a mythical land, a man and a young boy investigate a series of unusual occurrences.", "6.5/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTExODA0NzkxOTZeQTJeQWpwZ15BbWU3MDM0NDEyNzM@._V1_SY1000_CR0,0,675,1000_AL_.jpg"));
            Details.Add(Tuple.Create("Ocean Waves", "1993", "As a young man returns home after his first year away at college he recalls his senior year of high school and the iron-willed, big city girl that turned his world upside down.", "6.9/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BNTc5YmM4ZTQtMjc0Ny00NzZhLWJiZjktYmEzNGRkNTEzNWZmXkEyXkFqcGdeQXVyMzA3NDI5NTQ@._V1_SY1000_SX704_AL_.jpg"));
            Details.Add(Tuple.Create("Journey to Agartha", "2011", "A coming of age story involving young love and a mysterious music, coming from a crystal radio left as a memento by an absent father, that leads a young heroine deep into a hidden world.", "7.3/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMzZmZTQ4YjMtODgxYS00NDBlLTgyYWYtNWFkNjVhZGUyODRlL2ltYWdlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY1000_CR0,0,709,1000_AL_.jpg"));
            Details.Add(Tuple.Create("The Girl Who Leapt Through Time", "2006", "A high-school girl named Makoto acquires the power to travel back in time, and decides to use it for her own personal benefits. Little does she know that she is affecting the lives of others just as much as she is her own.", "7.8/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTg0ODYzODg4OV5BMl5BanBnXkFtZTcwNDM3NzAwMg@@._V1_.jpg"));
            Details.Add(Tuple.Create("5 Centimeters Per Second", "2007", "Told in three interconnected segments, we follow a young man named Takaki through his life as cruel winters, cold technology, and finally, adult obligations and responsibility converge to test the delicate petals of love.", "7.8/10", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTI4MDIyNzQyNl5BMl5BanBnXkFtZTcwNDcyNTk2MQ@@._V1_.jpg"));

            //creating each movie in the database, with the details from the details list
            for (int i = 0; i < Details.Count; i++)
            {
                var movie = new MovieDetails { URL = Details[i].Item5, Name = Details[i].Item1, Date = Details[i].Item2, Description = Details[i].Item3, Rating = Details[i].Item4 };
                await _connection.InsertAsync(movie);
            }
        }
    }
}
