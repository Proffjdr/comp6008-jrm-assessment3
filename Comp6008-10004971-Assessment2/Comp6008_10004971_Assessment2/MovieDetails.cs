﻿using SQLite;
using Xamarin.Forms;

namespace Comp6008_10004971_Assessment2
{
    public class MovieDetails
    {
        //SQL Primary Key
        [PrimaryKey, AutoIncrement]

        //contains movie ID
        public int MovieID { get; set; }

        //contains movie url
        public string URL { get; set; }

        //contains movie name
        public string Name { get; set; }

        //contains movie date
        public string Date { get; set; }

        //contains movie rating
        public string Rating { get; set; }

        //contains movie description
        public string Description { get; set; }
    }
}