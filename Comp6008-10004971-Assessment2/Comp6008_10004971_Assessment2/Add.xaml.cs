﻿using System;
using System.Collections.ObjectModel;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.Generic;

namespace Comp6008_10004971_Assessment2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Add : ContentPage
    {
        private int SelectedMovie;
        private SQLiteAsyncConnection _connection;
        private ObservableCollection<MovieDetails> MovieCollection;
        private List<MovieDetails> Movies;

        //Gets selected movie
        public Add(int i)
        {
            InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            SelectedMovie = i;
            Setup();
        }

        //Sets xaml text defaults for edit page
        async void Setup()
        {
            Movies = await _connection.Table<MovieDetails>().ToListAsync();
            if (SelectedMovie >= 0)
            {
                TitleTxt.Text = Movies[SelectedMovie].Name;
                DateTxt.Text = Movies[SelectedMovie].Date;
                RateTxt.Text = Movies[SelectedMovie].Rating;
                DescTxt.Text = Movies[SelectedMovie].Description;
                UrlTxt.Text = Movies[SelectedMovie].URL;
            }
        }

        //Add or update based on form type
        async void btnSubmit_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (SelectedMovie < 0)
                {
                    var movieAdd = new MovieDetails { URL = UrlTxt.Text, Name = TitleTxt.Text, Date = DateTxt.Text, Description = DescTxt.Text, Rating = RateTxt.Text };
                    await _connection.InsertAsync(movieAdd);

                    TitleTxt.Text = "";
                    DateTxt.Text = "";
                    RateTxt.Text = "";
                    DescTxt.Text = "";
                    UrlTxt.Text = "";
                }
                else
                {
                    Movies = await _connection.Table<MovieDetails>().ToListAsync();
                    MovieCollection = new ObservableCollection<MovieDetails>(Movies);

                    var movieUpdate = MovieCollection[SelectedMovie];
                    movieUpdate.Name = TitleTxt.Text;
                    movieUpdate.Date = DateTxt.Text;
                    movieUpdate.Rating = RateTxt.Text;
                    movieUpdate.Description = DescTxt.Text;
                    movieUpdate.URL = UrlTxt.Text;

                    await _connection.UpdateAsync(movieUpdate);
                }
                await DisplayAlert("MOVIES SAVED", "You movie has been saved", "OK");
            }
            catch
            {
                await DisplayAlert("Error", "There was a problem, please try again.", "OK");
            }
        }
    }
}
