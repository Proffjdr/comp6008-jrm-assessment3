﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using SQLite;
using System.Threading.Tasks;

namespace Comp6008_10004971_Assessment2
{
    public partial class MainPage : ContentPage
    {
        //number of columns (could be changed if desired).
        public static int NumberOfColumns = 4;

        //number of images
        public static int NumberOfImages = 20;

        //Connecting to database
        private SQLiteAsyncConnection _connection;

        //list of the movie details
        private List<MovieDetails> Movies;

        //this method begins the main programme
        public MainPage(string action, int NumImg)
        {
            InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            NumberOfImages = NumImg;
            Setup(action);
        }

        //This method determines whether or not to create a new list of movies
        private async void Setup(string action)
        {
            await Database.MakeTable();

            //await Database.DeleteMovies();

            Movies = await _connection.Table<MovieDetails>().ToListAsync();

            if (Movies.Count < 1)
            {
                await Database.CreateMovies();
            }

            await DisplayMovies(action);
        }

        //This method converts a URL into an URI image source
        private UriImageSource getURI(string url)
        {
            UriImageSource uri;
            try
            {
                uri = new UriImageSource { Uri = new Uri(url) };
                uri.CachingEnabled = false;
            }
            catch
            {
                uri = new UriImageSource { Uri = new Uri("http://www.51allout.co.uk/wp-content/uploads/2012/02/Image-not-found.gif") };
                uri.CachingEnabled = false;
            }
            return uri;
        }

        //This method creates the grid for displaying the images. It also creates the loading indicator and allows the images to be tapped.
        async Task DisplayMovies(string action)
        {
            //get the list of movies
            Movies = await _connection.Table<MovieDetails>().ToListAsync();

            //int to record how many images to actually display
            int display = NumberOfImages;

            //Determine max number of images to display
            if (display > 20)
            {
                display = 20;
            }
            if (display > Movies.Count)
            {
                display = Movies.Count;
            }

            //creates grid
            Grid grid = new Grid
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                RowSpacing = 0,
                ColumnSpacing = 0,

                //column definitions
                ColumnDefinitions = new ColumnDefinitionCollection
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)}
                },

                //row definitions
                RowDefinitions = new RowDefinitionCollection
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star)}
                }
            };

            //Add the grid to the stack layout
            StackMain.Children.Add
            (
                grid
            );

            //For each movie...
            for (var i = 0; i < display; i++)
            {
                //gets image
                var img = new Image
                {
                    Source = getURI(Movies[i].URL),
                    Aspect = Aspect.AspectFill,
                };

                //creates indicator
                var indicator = new ActivityIndicator
                {
                    IsRunning = true,
                };

                //works out the right row and column to place the indicator and images in
                var c = Convert.ToInt32(i % NumberOfColumns);
                var r = Convert.ToInt32(i / NumberOfColumns);

                //attaches the image to the relevant column and row
                Grid.SetColumn(img, c);
                Grid.SetRow(img, r);

                //attaches indicator to relevant column and row
                Grid.SetColumn(indicator, c);
                Grid.SetRow(indicator, r);

                //adds the indicator to the grid
                grid.Children.Add
                (
                    indicator
                );

                //adds the image to the grid
                grid.Children.Add
                (
                    img
                );

                //code for when the image is tapped
                var touchImage = new TapGestureRecognizer();

                //selects relevant form to open
                if (action == "Display")
                {
                    touchImage.Tapped += OpenDetailPage;
                }
                else if (action == "Edit")
                {
                    touchImage.Tapped += OpenEditPage;
                }
                else if (action == "Delete")
                {
                    touchImage.Tapped += OpenDeletePage;
                }

                img.GestureRecognizers.Add(touchImage);
            }
        }

        //This method gets the details of which movie was tapped and passes to the relevant method
        private int SelectedMovie(object sender)
        {
            var a = (int)((BindableObject)sender).GetValue(Grid.RowProperty) * NumberOfColumns;
            var b = (int)((BindableObject)sender).GetValue(Grid.ColumnProperty);

            return a + b;
        }

        //This method launches the detail page
        async void OpenDetailPage(object sender, EventArgs e)
        {
            int i = SelectedMovie(sender);
            await Navigation.PushAsync(new DetailPage(getURI(Movies[i].URL), Movies[i].Name, Movies[i].Rating, Movies[i].Description, Movies[i].Date));
        }

        //This method launches the edit page
        async void OpenEditPage(object sender, EventArgs e)
        {
            int i = SelectedMovie(sender);
            await Navigation.PushAsync(new Add(i));
        }

        //This method launches the delete function
        async void OpenDeletePage(object sender, EventArgs e)
        {
            try
            {
                int i = SelectedMovie(sender);
                var delete = await DisplayAlert("DELETE MOVIE", $"Are you sure you want to delete the movie {Movies[i].Name}?", "YES", "NO");
                if (delete == true)
                {

                    Movies = await _connection.Table<MovieDetails>().ToListAsync();
                    await _connection.DeleteAsync(Movies[i]);
                    await DisplayAlert("Complete", "Movie Deleted", "OK");
                }
            }
            catch
            {
                await DisplayAlert("Error", "Unable to delete movie", "OK");
            }
            await Navigation.PushAsync(new Menu());
        }
    }
}
