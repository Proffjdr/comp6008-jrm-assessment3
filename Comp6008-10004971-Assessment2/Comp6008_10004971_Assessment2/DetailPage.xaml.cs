﻿using Xamarin.Forms;
using SQLite;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace Comp6008_10004971_Assessment2
{
    public partial class DetailPage : ContentPage
    {
        public DetailPage(ImageSource uri, string title, string rating, string description, string date)
        {
            InitializeComponent();

            //displaying the image
            DetailImage.Source = uri;
            //displaying each parameter in a label
            DetailTitle.Text = $"Title: {title}";
            DetailRating.Text = $"Rating: {rating}";
            DetailDescription.Text = $"Description: {description}";
            DetailDate.Text = $"Date: {date}";
        }
    }
}
