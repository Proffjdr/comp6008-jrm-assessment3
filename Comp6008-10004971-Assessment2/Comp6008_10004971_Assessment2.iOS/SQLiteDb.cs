﻿using System;
using System.IO;
using SQLite;
using Xamarin.Forms;
using Comp6008_10004971_Assessment2.iOS;

[assembly: Dependency(typeof(SQLiteDb))]

namespace Comp6008_10004971_Assessment2.iOS
{
    public class SQLiteDb : ISQLiteDb
    {
        public SQLiteAsyncConnection GetConnection()
        {
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); 
            var path = Path.Combine(documentsPath, "MySQLite.db3");

            return new SQLiteAsyncConnection(path);
        }
    }
}

